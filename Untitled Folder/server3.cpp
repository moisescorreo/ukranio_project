#define MG_ENABLE_HTTP_STREAMING_MULTIPART 1
#include "mongoose.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <errno.h>

using namespace std;

static const char *s_http_port = "7200";
static struct mg_serve_http_opts s_http_server_opts;


static void handle_size(struct mg_connection *nc, struct http_message *hm) {
	struct timeval tiempo;
	tiempo.tv_sec=5;
    tiempo.tv_usec=0;
	char query[256];
	mg_get_http_var(&hm->body, "query", query,sizeof(query));	

	printf("Cadena: %s\n",query);
	printf("Puerto: %d\n",atoi(s_http_port));
	struct sockaddr_in msg_to_server_addr, client_addr;
	int s, num[2], res;

	s = socket(AF_INET, SOCK_DGRAM, 0);
	/* rellena la dirección del servidor */
	bzero((char *)&msg_to_server_addr, sizeof(msg_to_server_addr));
	msg_to_server_addr.sin_family = AF_INET;
	msg_to_server_addr.sin_addr.s_addr = inet_addr(query);
	msg_to_server_addr.sin_port = htons(atoi(s_http_port));

	/* rellena la direcciòn del cliente*/
	bzero((char *)&client_addr, sizeof(client_addr));
	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr = INADDR_ANY;

	int broadcastEnable=1;
	setsockopt(s, SOL_SOCKET, SO_BROADCAST ,&broadcastEnable, sizeof(broadcastEnable));
	setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char *)&tiempo, sizeof(tiempo));

	/*cuando se utiliza por numero de puerto el 0, el sistema se encarga de asignarle uno */
	client_addr.sin_port = htons(0);
	bind(s, (struct sockaddr *)&client_addr,sizeof(client_addr));
	num[0] = 2;
	num[1] = 5; /*rellena el mensaje */
	sendto(s, (char *)num, 2 * sizeof(int), 0, (struct sockaddr *) &msg_to_server_addr, sizeof(msg_to_server_addr));
	socklen_t clilen = sizeof(client_addr);
	/* se bloquea esperando respuesta */
	int i;
	memset(query, 0, 256);
	while(1){
		i = recvfrom(s, (char *)&res, sizeof(int), 0, (struct sockaddr *)&client_addr, &clilen);
		if (i < 0){
        	if (errno == EWOULDBLOCK){
                fprintf(stderr, "Tiempo para recepción transcurrido\n");
                break;
            }
            else
        	fprintf(stderr, "Error en recvfrom\n");
        }
		//printf("Solicitud de %s\n",inet_ntoa(client_addr.sin_addr));
		
		sprintf(query, "%s %s<br>",query, inet_ntoa(client_addr.sin_addr));
		//memcpy(servidores+strlen(servidores),query,strlen(query));
		printf("Cadena enviada: %s\n", query);

		//mg_send_head(nc,200,strlen(query), "Content-Type: text/plain");
		//mg_printf(nc, "%s", query);
	}
	printf("Termino %s\n",query);
	mg_send_head(nc,200,strlen(query), "Content-Type: text/plain");
	//mg_send_head(nc,240,strlen(servidores), "Content-Type: text/plain");
	mg_printf(nc, "%s", query);
	close(s);
	
	
}


static void ev_handler(struct mg_connection *nc, int ev, void *p) {
	char query[256];
 	struct http_message *hm = (struct http_message *) p;


	if (ev == MG_EV_HTTP_REQUEST) {
		if (mg_vcmp(&hm->uri, "/search") == 0) { 
			
			mg_get_http_var(&hm->body, "query", query,sizeof(query));
			printf("Cadena introducida: %s\n",query);

		    handle_size(nc, hm);  
		}else{
			mg_serve_http(nc, (struct http_message *) p, s_http_server_opts);
		}
	}

}

int main(void) {
	struct mg_mgr mgr;
	struct mg_connection *nc;
	mg_mgr_init(&mgr, NULL);

	printf("Starting web server on port %s\n", s_http_port);
	nc = mg_bind(&mgr, s_http_port, ev_handler);
	if (nc == NULL) {
		printf("Failed to create listener\n");
		return 1;
	}
	// Set up HTTP server parameters
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = "www"; // Serve current directory
	s_http_server_opts.enable_directory_listing = "yes";
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
	}
	mg_mgr_free(&mgr);
	return 0;
}
